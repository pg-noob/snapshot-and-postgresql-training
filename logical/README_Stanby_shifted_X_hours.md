# Guide to building a postgreSQL standby  but shifted by 6 hours from a snapshot



## 1. apply Readme.md

apply the previous “Guide” Readme.md up to and including point 8

....

## 9. zfs clone (ctrl-b 0)

choose your prefered snapshot 

```bash
zfs clone zfspool/primary@for_standby1 zfspool/standby 
touch /zfspool/standby/data/standby.signal
chown -R 999.999 /zfspool/standby/wal/
rm /zfspool/standby/data/postmaster.pid
```

## 10. start standby container  (ctrl-b 2)
```bash
docker-compose up -d postgres_standby
docker exec -it pg_standby bash
```

## 11. now start instance of standby (ctrl-b 2)

```bash
su postgres
pg_ctl start
```

normally you should find in your log :
entering standby mode
redo starts at 0/5E6BE528
consistent recovery state reached at 0/5E6BE610
database system is ready to accept read only connections

error :
consistent recovery state reached at 0/5E6BE610
invalid record length at 0/5E6BE610: wanted 24, got 0

error :
 database system is ready to accept read only connections
2024-05-03 15:45:13.928 UTC [300] FATAL:  could not connect to the primary server: connection to server at "10.5.0.5", port 5432 failed: Connection refused
                Is the server running on that host and accepting TCP/IP connections?

  did you stop your primary seveur?
  
## 12. check monitor 
```bash
/scripts/compare.sh #just compare diff to 2 sequences, relauch python3 import1.py 
```

## 13. si vous voulez couper la standby (ctrl-b 2)

## IX)  tools ctrl-b 0
```bash

zpool import -f zfspool 
for restore after destroy 
```


## X)Clean all

```bash

 docker-compose down
 zfs destroy -R zfspool/standby
 zfs destroy -R zfspool/primary
 zpool  destroy zfspool
 sudo losetup -d /dev/loop100
 rm  /data/data.zpool
 zfs list 
```

## XI) postgres commands to monitor application status
```bash
 psql -h "10.5.0.5" -x -c "SELECT * FROM pg_stat_replication;" #voir l'état de la réplication
 
```
or
```bash
 docker exec -it pg_primary su -c "psql -x -c \"SELECT * FROM pg_stat_replication;\"" postgres
 ```
 ```bash
 docker exec -it pg_standby su -c "psql -x -c \"SELECT * FROM pg_stat_wal_receiver;\"" postgres
 ```
 
 ```bash
 /scripts/compare.sh #just compare diff to 2 sequences, relauch python3 import1.py 
 docker exec -it pg_primary su -c "/scripts/compare.sh" root
 zfs list -t snapshot
 ```
 
 ```bash
  docker exec -it pg_primary su -c "psql -x mondb -c \"SELECT id,col_previous_id,col_id from test_replication order by id desc limit 1;\"" postgres \ 
  && docker exec -it pg_standby su -c "psql -x mondb -c \"SELECT id,col_previous_id,col_id from test_replication order by id desc limit 1;\"" postgres     
 ```
 
 psql -x mondb -c "SELECT l.id,l.col_previous_id,l.col_id from test_replication l left join  test_replication r on r.id=l.col_previous_id where r.id is null ";
 