# Guide to building a postgreSQL standby from a snapshot 

In this guide, we'll try to remount a standby from a snapshot with backup.

This guide provides step-by-step instructions for setting up PostgreSQL containers using Docker and ZFS for data management.

This tutorial works for databases that are snapshot-only. 
But in very large infrastructures, it's difficult to snapshot just one DB. 
And synchronizing all databases via pg_start_backup is not possible (e.g. 100 postgreSQL instances in 100 VMs). 
If this is your case, see README_Snapshot_without_backup.md

## 1. Install Docker

```bash
apt-get update
apt install docker.io docker-compose tmux
./start_tmux.sh  #start 3 terms - switch with ctrl-b 
```

## 2. install zfs (ctrl-b 0)

```bash
apt install zfsutils-linux
```

or If, like me, you are using WSL2, you will need to recompile the kernel.
It's all in this incredible Tuto https://wsl.dev/wsl2-kernel-zfs/ thanks  Nunix

## 3. create pool for your data

```bash
truncate -s 10G /data/data.zpool		#create file 
losetup /dev/loop100 /data/data.zpool	#associate file to drive
zpool create zfspool /dev/loop100		#create pool 
zfs create zfspool/primary				#create primary mount
mkdir /zfspool/primary/data				#create data  mount for your container
mkdir /zfspool/primary/wal				#same for  mount for your container
```

```bash
zfs create zfspool/wal_archive			#mounted, for backup and restore on standby
chmod 777 /zfspool/wal_archive			#not used in production, 
```

```bash
zfs list
```
Expected output:
```
zfspool               176K  9.20G       24K  /zfspool
zfspool/primary        25K  9.20G       25K  /zfspool/primary
zfspool/wal_archive    24K  9.20G       24K  /zfspool/wal_archive
```

## 4. create your primary container (ctrl-b 1)
```bash
docker-compose up -d postgres_primary
```

## 5. on your container (ctrl-b 1)
```bash
docker exec -it pg_primary bash
/scripts/install.sh 
su postgres
pg_ctl start
psql -c "CREATE USER rep_user REPLICATION LOGIN ENCRYPTED PASSWORD 'rep_password';"
psql -c "create database mondb;"
```

## 6. on your main (ctrl-b 0)

```bash
zfs snapshot -r zfspool/primary@after-createdb
zfs list -t snapshot
```

## 7. lauch  load data (ctrl-b 3)

```bash
python3 import1.py # this script loads 10 millions lines and snapshots every 100,000 lines
#wait 100K for first snapshot 
```

## 8. Make your snapshots for standby 1 (ctrl-b 0)

```bash
zfs list -t snapshot
 
docker exec -it pg_primary su -c "psql -c \"SELECT pg_start_backup('for_standby1');\"" postgres
zfs snapshot -r zfspool/primary@for_standby1
docker exec -it pg_primary su -c 'psql -c "SELECT pg_stop_backup();"' postgres
 
 
zfs list -t snapshot
```

## 9. zfs clone (ctrl-b 0)

```bash
zfs clone zfspool/primary@for_standby1 zfspool/standby 
touch /zfspool/standby/data/standby.signal
chown -R 999.999 /zfspool/standby/wal/
rm /zfspool/standby/data/postmaster.pid
```

## 10. start standby container  (ctrl-b 2)
```bash
docker-compose up -d postgres_standby
docker exec -it pg_standby bash
```

## 11. now start instance of standby (ctrl-b 2)

```bash
su postgres
pg_ctl start
```

normally you should find in your log :
entering standby mode
redo starts at 0/5E6BE528
consistent recovery state reached at 0/5E6BE610
database system is ready to accept read only connections

error :
consistent recovery state reached at 0/5E6BE610
invalid record length at 0/5E6BE610: wanted 24, got 0

error :
 database system is ready to accept read only connections
2024-05-03 15:45:13.928 UTC [300] FATAL:  could not connect to the primary server: connection to server at "10.5.0.5", port 5432 failed: Connection refused
                Is the server running on that host and accepting TCP/IP connections?

  did you stop your primary seveur?
  
## 12. check monitor 
```bash
/scripts/compare.sh #just compare diff to 2 sequences, relauch python3 import1.py 
```

## 13. si vous voulez couper la standby (ctrl-b 2)

## IX)  tools ctrl-b 0
```bash

zpool import -f zfspool 
for restore after destroy 
```


## X)Clean all

```bash

 docker-compose down
 zfs destroy -R zfspool/standby
 zfs destroy -R zfspool/primary
 zpool  destroy zfspool
 sudo losetup -d /dev/loop100
 rm  /data/data.zpool
 zfs list 
```

## XI) postgres commands to monitor application status
```bash
 psql -h "10.5.0.5" -x -c "SELECT * FROM pg_stat_replication;" #voir l'état de la réplication
 
```
or
```bash
 docker exec -it pg_primary su -c "psql -x -c \"SELECT * FROM pg_stat_replication;\"" postgres
 ```
 ```bash
 docker exec -it pg_standby su -c "psql -x -c \"SELECT * FROM pg_stat_wal_receiver;\"" postgres
 ```
 
 ```bash
 /scripts/compare.sh #just compare diff to 2 sequences, relauch python3 import1.py 
 docker exec -it pg_primary su -c "/scripts/compare.sh" root
 zfs list -t snapshot
 ```
 
 ```bash
  docker exec -it pg_primary su -c "psql -x mondb -c \"SELECT id,col_previous_id,col_id from test_replication order by id desc limit 1;\"" postgres \ 
  && docker exec -it pg_standby su -c "psql -x mondb -c \"SELECT id,col_previous_id,col_id from test_replication order by id desc limit 1;\"" postgres     
 ```
 
 psql -x mondb -c "SELECT l.id,l.col_previous_id,l.col_id from test_replication l left join  test_replication r on r.id=l.col_previous_id where r.id is null ";
 