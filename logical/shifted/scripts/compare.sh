#!/bin/bash
# Connection configuration for the primary server
HOST1="10.5.0.5"
DBNAME1=$POSTGRES_DB
USER1=$POSTGRES_USER
PASSWORD1=$POSTGRES_PASSWORD

# Connection configuration for the standby server
HOST2="10.5.0.6"
DBNAME2=$POSTGRES_DB
USER2=$POSTGRES_USER
PASSWORD2=$POSTGRES_PASSWORD

# SQL query to get the current value of the sequence
SQL="SELECT last_value FROM test_replication_id_seq;"

# Exporting passwords to avoid prompts
export PGPASSWORD=$POSTGRES_PASSWORD
export PGPASSWORD=$POSTGRES_PASSWORD

# Infinite loop to compare the sequences every second
while true; do
    # Execute the query on the primary server
    VALUE1=$(psql -h $HOST1 -U $USER1 -d $DBNAME1 -t -c "$SQL" | xargs)
    
    # Execute the query on the standby server
    VALUE2=$(psql -h $HOST2 -U $USER2 -d $DBNAME2 -t -c "$SQL" | xargs)

    # Calculate and display the difference
    DIFF=$(($VALUE1-$VALUE2))
    echo "Difference between sequences: $DIFF"

    # Pause for one second
    sleep 1
done
