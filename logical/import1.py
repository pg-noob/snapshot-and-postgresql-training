import subprocess
import random
import lorem
import time

import psycopg2
from psycopg2 import sql
from tqdm import tqdm

# Connexion à la base de données PostgreSQL
conn = psycopg2.connect(
    dbname="mondb", user="postgres", password="mysecretpassword", host="localhost", port="5432"
)
cur = conn.cursor()



total_inserts = 10000000
snapshot_size = 100000
batch_size = 1

# Démarrage du compteur de temps
start_time = time.time()

# Création de la table avec 30 colonnes de types variés
cur.execute("""
CREATE TABLE IF NOT EXISTS test_replication (
    id SERIAL PRIMARY KEY,
    col_id bigint,
    col_previous_id bigint,
    col_int INTEGER,
    col_float FLOAT,
    col_text TEXT,
    col_bool BOOLEAN,
    col_date DATE,
    col_time TIME,
    col_datetime TIMESTAMP,
    col_varchar VARCHAR(50),
    col_char CHAR(10),
    col_smallint SMALLINT,
    col_bigint BIGINT,
    col_numeric NUMERIC(10, 2),
    col_real REAL,
    col_double DOUBLE PRECISION,
    col_money MONEY,
    col_bytea BYTEA,
    col_uuid UUID,
    col_cidr CIDR,
    col_inet INET,
    col_mac MACADDR,
    col_bit BIT(3),
    col_varbit BIT VARYING(5),
    col_xml XML,
    col_json JSON,
    col_jsonb JSONB,
    col_array TEXT[],
    col_tsvector TSVECTOR,
    col_tsquery TSQUERY,
    col_point POINT
)
""")
conn.commit()
inserted_id=-1
# Insertion des données
for i in tqdm(range(total_inserts), desc="Inserting data"):
    cur.execute(sql.SQL("""
    INSERT INTO test_replication ( col_id,col_previous_id,
        col_int, col_float, col_text, col_bool, col_date, col_time,
        col_datetime, col_varchar, col_char, col_smallint, col_bigint,
        col_numeric, col_real, col_double, col_money, col_bytea, col_uuid,
        col_cidr, col_inet, col_mac, col_bit, col_varbit, col_xml, col_json,
        col_jsonb, col_array, col_tsvector, col_tsquery, col_point
    ) VALUES (
        %(col_id)s,%(col_previous_id)s,%(col_int)s, %(col_float)s, %(col_text)s, %(col_bool)s, CURRENT_DATE,
        CURRENT_TIME, CURRENT_TIMESTAMP, %(col_varchar)s, 'fixedtext', %(col_smallint)s,
        %(col_bigint)s, %(col_numeric)s, %(col_real)s, %(col_double)s, '12.34',
        'binary data'::bytea, 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
        '192.168.100.128/25', '192.168.100.128', '08:00:2b:01:02:03', B'101',
        B'10101', '<root></root>', '{"key": "value"}', '{"key": "value"}',
        ARRAY['element1', 'element2'], to_tsvector('simple', 'text'), to_tsquery('simple', 'text'),
        POINT(1, 2)
    )
    RETURNING id
    """), {
        'col_id' : i+1,
        'col_previous_id':inserted_id,
        'col_int': random.randint(1, 1000),
        'col_float': random.random() * 100,
        'col_text': lorem.text()[:2500],
        'col_bool': random.choice([True, False]),
        'col_varchar': lorem.text()[:50],
        'col_smallint': random.randint(1, 100),
        'col_bigint': random.randint(1, 10000),
        'col_numeric': random.random() * 100,
        'col_real': random.random() * 100,
        'col_double': random.random() * 100
    })
    # Commit après chaque lot de 1000 enregistrements
    inserted_id = cur.fetchone()[0]
    if (i + 1) % batch_size == 0:
        conn.commit()
        print(f"Commit done at {i + 1} records")
    if (i + 1) % snapshot_size == 0:
        result = subprocess.run(["zfs","snapshot","-r",f"zfspool/primary@after-{i+1}"], capture_output=True, text=True)
        print(f"snapshot {i + 1} records")
        
        
# Temps écoulé
elapsed_time = time.time() - start_time
print(f"Total time elapsed: {elapsed_time:.2f} seconds")

# Valider les changements
conn.commit()

# Valider les changements
conn.commit()

# Fermer la connexion
cur.close()
conn.close()
