#!/bin/bash

# Lance tmux
tmux new-session -d -s mySession -n main

# Ajoute les autres fenêtres
tmux new-window -t mySession:1 -n primary
tmux new-window -t mySession:2 -n standby
tmux new-window -t mySession:3 -n import

# Attache à la session
tmux attach-session -t mySession
