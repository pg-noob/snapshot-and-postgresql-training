#!/bin/bash

# Connection variables



# Threshold for lag (in bytes)
MAX_LAG=1048576  # Example: 1MB
while true; do
	# Fetch the latest WAL LSN (Log Sequence Number) from the primary server
	master_lsn=$(psql -h 10.5.0.5 -tAc "SELECT pg_current_wal_lsn();")

	# Fetch the latest WAL LSN received by the standby server
	standby_lsn=$(psql "$STANDBY_DB" -tAc "SELECT latest_end_lsn FROM pg_stat_wal_receiver;")

	# Convert LSN to logical segment number for comparison
	master_lsn_num=$(echo $master_lsn | cut -d '/' -f2)
	master_lsn_num=$((16#$master_lsn_num))  # Convert from hexadecimal to decimal

	standby_lsn_num=$(echo $standby_lsn | cut -d '/' -f2)
	standby_lsn_num=$((16#$standby_lsn_num))  # Convert from hexadecimal to decimal

	# Calculate the lag
	lag=$(($master_lsn_num - $standby_lsn_num))

	# Compare the lag to the threshold and output an alert if necessary
	if [ "$lag" -gt "$MAX_LAG" ]; then
		echo "Alert: Replication lag is too high! Lag is ${lag} bytes."
	else
		echo "Replication is within acceptable (${lag}). "
	fi
	sleep 1
done