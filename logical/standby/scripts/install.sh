rm /var/lib/postgresql/data/postmaster.pid
rm /var/lib/postgresql/data/recovery.done
rm -rf /var/lib/postgresql/data/pg_xlog/*
rm -rf /var/lib/postgresql/data/pg_log/*


rm /var/lib/postgresql/data/postgresql.auto.conf
ln -s /etc/postgresql/postgresql.auto.conf /var/lib/postgresql/data/postgresql.auto.conf